
# Data

## Object model

### agsiDataCase

[agsiDataCase](/Data_agsiDataCase) is a dictionary of the case IDs referenced by other objects, if applicable. Case IDs can be used to where the valid usage of a parameter needs to be defined, e.g. for a specific type of analysis. Case IDs may also be used to faciliate alternative reporting of properties, e.g. a statistical summary that ignores outliers.

The parent object of [agsiDataCase](/Data_agsiDataCase) is [agsiData](/Data_agsiData)

[agsiDataCase](/Data_agsiDataCase) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](/Data_agsiDataPropertyValue)
- [agsiDataParameterValue](/Data_agsiDataParameterValue)

[agsiDataCase](/Data_agsiDataCase) has the following additional attributes:


#### caseID
Identifying code for a defined case. Referenced by [agsiDataParameterValue](/Data_agsiDataParameterValue) and/or [agsiData](/Data_agsiData) PropertyValue   
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``EC7PILE``

#### description
Short description of the case, e.g. use case for a parameter.  
*Type:* string, Text  
*Condition:* Required  
*Example:* ``EC7 Pile design``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``See alternate cases for LDSA pile design and retaining wall design.``

