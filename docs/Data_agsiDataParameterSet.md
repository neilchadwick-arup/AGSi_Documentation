
# Data

## Object model

### agsiDataParameterSet

[agsiDataParameterSet](/Data_agsiDataParameterSet) objects are containers for subsets of parameter data, with each subset typically corresponding to the property data for a particular agsModelElement or agsFeature object.

The parent object of [agsiDataParameterSet](/Data_agsiDataParameterSet) is [agsiData](/Data_agsiData)

[agsiDataParameterSet](/Data_agsiDataParameterSet) contains the following embedded child objects: 

- [agsiDataParameterValue](/Data_agsiDataParameterValue)

[agsiDataParameterSet](/Data_agsiDataParameterSet) has the following additional attributes:


#### parameterSetID
Identifier for this set of data. This will be referenced by other objects such as agsModelElement and agsFeature as required. Use of a simple code is recommended, such as the geology code if no further subdivision of the data is required.   
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``LC``

#### description
Short description identifying the data set.  
*Type:* string, Text  
*Example:* ``London Clay``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

#### agsiDataParameterValue
See [agsiDataParameterValue](/Data_agsiDataParameterValue)  
*Type:* array, [agsiDataParameterValue](/Data_agsiDataParameterValue) object(s)  


