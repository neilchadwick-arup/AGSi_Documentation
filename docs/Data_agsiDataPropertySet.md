
# Data

## Object model

### agsiDataPropertySet

[agsiDataPropertySet](/Data_agsiDataPropertySet) objects are containers for subsets of property data, with each subset typically corresponding to the property data for a particular agsModelElement or agsFeature object.

The parent object of [agsiDataPropertySet](/Data_agsiDataPropertySet) is [agsiData](/Data_agsiData)

[agsiDataPropertySet](/Data_agsiDataPropertySet) contains the following embedded child objects: 

- [agsiDataPropertyValue](/Data_agsiDataPropertyValue)

[agsiDataPropertySet](/Data_agsiDataPropertySet) has the following additional attributes:


#### propertySetID
Identifier for this set of data. This will be referenced by other objects such as agsModelElement and agsFeature as required. Use of a simple code is recommended, such as the geology code if no further subdivision of the data is required.   
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``LC``

#### description
Short description identifying the data set.  
*Type:* string, Text  
*Example:* ``London Clay``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

#### agsiDataPropertyValue
See [agsiDataPropertyValue](/Data_agsiDataPropertyValue)  
*Type:* array, [agsiDataPropertyValue](/Data_agsiDataPropertyValue) object(s)  


