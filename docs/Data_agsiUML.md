# Data Model UML object diagram and Spreadsheet

```plantuml
@startuml

title UML Diagram - For comment

header
Version???
endheader

hide members

agsiData "XXX" --|> "YYY" agsiDataPropertySet
agsiData --|> agsiDataParameterSet
agsiData --|> agsiDataCase
agsiData --|> agsiDataCode
agsiDataPropertySet --|> agsiDataPropertyValue
agsiDataParameterSet --|> agsiDataParameterValue
agsiDataCase .. agsiDataPropertyValue
agsiDataCode .. agsiDataPropertyValue
agsiDataCase .. agsiDataParameterValue
agsiDataCode .. agsiDataParameterValue


object agsiData #LightGreen {
  +dataset: string
  +description: string
  codeDictionary = string
  agsiDataParameterSet = array
  agsiDataPropertySet = array
  agsiDataCase = array
  agsiDataCode = array
}
object agsiDataPropertySet {
  propertySetID = string
  description = string
  remarks = string
  agsiDataPropertyValue = array
}
object agsiDataParameterSet {
  parameterSetID = string
  description = string
  remarks = string
  agsiDataParameterValue = array
}
object agsiDataPropertyValue {
  codeID = string
  caseID = string
  valueMin = number
  valueMax = number
  valueMean = number
  valueStdDev = number
  valueCount = number
  valueText = string
  valueProfileIndVarCodeID = string
  valueProfileCoordinates = array
  remarks = string
}
object agsiDataParameterValue {
  codeID = string
  caseID = string
  valueNumeric = number
  valueText = string
  valueProfileIndVarCodeID = string
  valueProfileCoordinates = array
  remarks = string
}
object agsiDataCode {
  codeID = string
  description = string
  units = string
  isNonStandard = boolean
  remarks = string
}
object agsiDataCase {
  caseID = string
  description = string
  remarks = string
}



@enduml
```

## Data Model Source Spreadsheet

The spreadsheet below is hosted on Google Docs. It contains the current master data model.

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT-wBsmsGPGbE79vrqNdx6u2Mvb5dl5pY4oC1BRS1K3GNfyyf0XmBan9SAjFRPxm0l936I6SZ98yRpB/pubhtml?widget=true&amp;headers=false" height="600" width="900"></iframe>
</figure>
