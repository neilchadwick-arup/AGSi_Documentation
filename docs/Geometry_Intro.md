# Geometry

## Introduction

### Overview

The 'Geometry' part of the schema is the repository for geometry data
associated with model objects, most commonly [agsiModelElement](/Model_agsiModelElement)
objects but also some [agsiModelBoundary](/Model_agsiModelBoundary) objects.

The geometry required by the [agsiModelElement](/Model_agsiModelElement) or [agsiModelBoundary](/Model_agsiModelBoundary) objects
is identified by reference to the *[geometryID](/Model_agsiModelElement#geometryid)* attribute for the
relevant target geometry object.

The reason for separating geometry from the model and linking it by reference
is that it makes it possible to re-use the same geometrical data for
a number of different model elements, thus avoiding duplication of
the same data. It also means that the model part of the schema can
be lightweight and easy to follow.

There are several different types of geometry objects, as listed below.

Guidance on usage for geometry objects is included in the guidance for Model.

!!! Todo
    Develop these basic descriptions, maybe including images where useful.
    May become a new page eventually.

### agsiGeometryFromFile

An [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object is a pointer to geometry data contained within an external file, such as a CAD or model file. This object also includes metadata describing the file being referenced

### agsiGeometryLayer

An [agsiGeometryLayer](/Geometry_agsiGeometryLayer) object defines an element as the volume between two horizontal planes at specified elevations.

### agsiGeometryExpHoleSet

An [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets,  e.g. sets for different investigations, or different types of hole. The data for the holes is included via the embedded [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) objects. Geologicical logging data is not included in these objects: see [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) and [agsiGeometryColumn](/Geometry_agsiGeometryColumn).

### agsiGeometryExpHole

An [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object provides the geometric data for a single exploratory holes (boreholes, trial pits, CPT etc.). In addition, some limited additional metadata for the hole is included alngside an optional link to additional data defined in and agsDataPropertySet object. This could be used to link additional data such as SPT results. Geologicical logging data is not included here: see [agsiGeometryColumn](/Geometry_agsiGeometryColumn).

### agsiGeometryColumnSet

An [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) object is set of stratigraphical column segments, typically used to represent geology in exploratory holes. Sets are expected to align with geological units, although additional or alternative subdivisions are permitted. The data for the columns is included via the embedded [agsiGeometryColumn](/Geometry_agsiGeometryColumn) objects. For information on the exploratory holes themselves see [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) and [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole).

### agsiGeometryColumn

An [agsiGeometryColumn](/Geometry_agsiGeometryColumn) object provides the geometric data for a single stratigraphical column segment. In addition, essential data such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object, but this is not essential.

### agsiGeometryVolFromSurfaces

An [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object defines an element as the volume between top and bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects. The volume is should be considered not to exist where the top and bottom surfaces are coexistent, or the top lies below the bottom. Volumes that fold back on themselves cannot be represented by this method.

### agsiGeometryAreaFromLines

An [agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) object defines an element as the area between ltop and bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. The volume is should be considered not to exist where the top and bottom lines are coexistent, or the top lies below the bottom. Areas that fold back on themselves cannot be represented by this method.

### agsiGeometryLine

An [agsiGeometryLine](/Geometry_agsiGeometryLine) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, t#he plane that the lines lies in will be determined by the referencing object. Lines must comprise straight segments. Curves are not supported.
