
# Geometry

## Object model

### agsiGeometry

The [agsiGeometry](/Geometry_agsiGeometry) object is a wrapper for all of the geometry data for a root dataset. It is required in all cases where geometry data is provided. There should be only one [agsiGeometry](/Geometry_agsiGeometry) object per root dataset. All other objects in the Geometry part of the schema are embedded within [agsiGeometry](/Geometry_agsiGeometry).

The parent object of [agsiGeometry](/Geometry_agsiGeometry) is root

[agsiGeometry](/Geometry_agsiGeometry) has the following additional attributes:


#### geometrySetID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but there may be cases where it is may be beneficial to include this to help with data control and integrity.  
*Type:* string, Identifier  
*Example:* ``f402948e-d5f4-4b1c-923b-2ce8578bb5c3``

#### description
Similar usage to geometrySetID but intended to be a more verbose description.  
*Type:* string, Text  
*Example:* ``Geometry data for Geological Model: sitewide``

#### agsiGeometryFromFile
See [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile)  
*Type:* array, [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects  


#### agsiGeometryLayer
See [agsiGeometryLayer](/Geometry_agsiGeometryLayer)  
*Type:* array, [agsiGeometryLayer](/Geometry_agsiGeometryLayer) objects  


#### agsiGeometryExpHoleSet
See [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet)  
*Type:* array, [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) objects  


#### agsiGeometryColumnSet
See [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet)  
*Type:* array, [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) objects  


#### agsiGeometryVolFromSurfaces
See [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces)  
*Type:* array, [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) objects  


#### agsiGeometryAreaFromLines
See [agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines)  
*Type:* array, [agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) objects  


#### agsiGeometryLine
See [agsiGeometryLine](/Geometry_agsiGeometryLine)  
*Type:* array, [agsiGeometryLine](/Geometry_agsiGeometryLine) objects  


#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

