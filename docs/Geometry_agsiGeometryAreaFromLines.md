
# Geometry

## Object model

### agsiGeometryAreaFromLines

An [agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) object defines an element as the area between ltop and bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. The volume is should be considered not to exist where the top and bottom lines are coexistent, or the top lies below the bottom. Areas that fold back on themselves cannot be represented by this method.

The parent object of [agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)
- [agsiGeometryLine](/Geometry_agsiGeometryLine)
- [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile)

[agsiGeometryAreaFromLines](/Geometry_agsiGeometryAreaFromLines) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``GEOM-SECTIONAA-LC``

#### description
Short description of geometry defined here  
*Type:* string, Text  
*Example:* ``Section AA, London Clay ``

#### topGeometryID
Identifies the geometry for top line, linking to [agsiGeometryLine](/Geometry_agsiGeometryLine) or [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object.  
*Type:* string, Reference to geometryID for an [agsiGeometryLine](/Geometry_agsiGeometryLine) or [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object  
*Condition:* Required  
*Example:* ``GEOM-SECTIONAA-LC-TOP``

#### bottomGeometryID
Identifies the geometry for bottom line, normally linking to an [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object. Use is strongly recommended as lower boundary of element may be ambiguous otherwise, i.e. it would rely on rely on the presence of an element or boudary below.  
*Type:* string, Reference to geometryID for an [agsiGeometryLine](/Geometry_agsiGeometryLine) or [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object  
*Condition:* Recommended  
*Example:* ``GEOM-SECTIONAA-LMG-TOP``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

