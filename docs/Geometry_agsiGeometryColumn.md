
# Geometry

## Object model

### agsiGeometryColumn

An [agsiGeometryColumn](/Geometry_agsiGeometryColumn) object provides the geometric data for a single stratigraphical column segment. In addition, essential data such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object, but this is not essential.

The parent object of [agsiGeometryColumn](/Geometry_agsiGeometryColumn) is [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet)

[agsiGeometryColumn](/Geometry_agsiGeometryColumn) has associations (reference links) with the following objects: 

- [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole)

[agsiGeometryColumn](/Geometry_agsiGeometryColumn) has the following additional attributes:


#### topCoordinate
Co-ordinates of the top of the column segment, as a co-ordinate triple (Easting (or x), Northing (y), Elevation (z)).  
*Type:* array, See special guidance on required format.  
*Condition:* Required  
*Example:* ``[525275.5,181543.2,6.3]``

#### vertBottomElevation
For a vertical column only, elevation of bottom of segment. For non vertical segments use bottomCoordinates  
*Type:* array  
*Condition:* Required if bottomCoordinates not used  
*Example:* ``-28.4``

#### bottomCoordinate
Co-ordinates of the bottom of the segment, as a co-ordinate triple (Easting (or x), Northing (y), Elevation (z)).   
*Type:* array, See special guidance on required format.  
*Condition:* Required if vertBottomElevation not used  
*Example:* ``[525275.5,181543.2,-28.4]``

#### expHoleName
Name of the relevant exploratory hole. Allows this information to be conveyed when the exploraratory holes are not modelled as objects.  
*Type:* string, Text  
*Example:* ``A/BH01``

#### expHoleID
Link to [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object for the relevant exploratory hole. Reference should be made to the expHoleID, not the name.  
*Type:* string, Reference to [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) expHoleID  
*Example:* ``BH01``

#### description
Geological description, or other type of description depending on use of column  
*Type:* string, Text  
*Example:* ``Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (<0.1m thick)``

#### legendCode
Legend code  
*Type:* string, AGS ABBR legend code  
*Example:* ``201``

#### geologyCode
Geology code  
*Type:* string, From project code list  
*Example:* ``LC``

#### geologyCode2
2nd Geology code  
*Type:* string, From project code list  
*Example:* ``A2``

#### propertySetID
Link to additional property data for this hole  
*Type:* string, Reference to agsDataPropertySet propertySetID  
*Example:* ``PROP-A/BH01``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Original name on logs: BH1``

