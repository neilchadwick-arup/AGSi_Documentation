
# Geometry

## Object model

### agsiGeometryColumnSet

An [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) object is set of stratigraphical column segments, typically used to represent geology in exploratory holes. Sets are expected to align with geological units, although additional or alternative subdivisions are permitted. The data for the columns is included via the embedded [agsiGeometryColumn](/Geometry_agsiGeometryColumn) objects. For information on the exploratory holes themselves see [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) and [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole).

The parent object of [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) contains the following embedded child objects: 

- [agsiGeometryColumn](/Geometry_agsiGeometryColumn)

[agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)

[agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``GEOLCOL-GI-A-LC``

#### description
Short description of the set of columns defined here  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``London Clay (2019 GI Package A)``

#### columnType
Type of informaiton conveyed in this column set.  
*Type:* string, Standard term  
*Example:* ``Geology``

#### agsiGeometryColumn
See [agsiGeometryColumn](/Geometry_agsiGeometryColumn)  
*Type:* array, [agsiGeometryColumn](/Geometry_agsiGeometryColumn) objects  


#### remarks
Additional remarks, if required  
*Type:* string, Text  


