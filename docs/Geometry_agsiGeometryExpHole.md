
# Geometry

## Object model

### agsiGeometryExpHole

An [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object provides the geometric data for a single exploratory holes (boreholes, trial pits, CPT etc.). In addition, some limited additional metadata for the hole is included alngside an optional link to additional data defined in and agsDataPropertySet object. This could be used to link additional data such as SPT results. Geologicical logging data is not included here: see [agsiGeometryColumn](/Geometry_agsiGeometryColumn).

The parent object of [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) is [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet)

[agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) has associations (reference links) with the following objects: 

- Investigation?

[agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) has the following additional attributes:


#### expHoleID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. Not necessarily the same as the original hole ID (see name) as exploratory hole identifiers should be unique within a dataset. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``A/BH01``

#### name
Current name or ID of the exploratory hole for general use.  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``BH01``

#### topCoordinate
Co-ordinates of the top of the exploratory hole, as a co-ordinate triple (Easting (or x), Northing (y), Elevation (z)).  
*Type:* array, See special guidance on required format.  
*Condition:* Required unless profileCoordinates used  
*Example:* ``[525275.5,181543.2,15.25]``

#### verticalHoleDepth
Final depth of exploraratory hole for vertical holes only. For non-vertical or non-straight holes use profileCoordinates attribute instead  
*Type:* number  
*Condition:* Required unless profileCoordinates used  
*Example:* ``25``

#### profileCoordinates
Co-ordinates of the line of the exploratory hole, i.e. top, bottom and intermediate changes in direction if required. Input as ordered list of co-ordinate triples (x,y,z) starting at the top. Must be used for holes that are not vertical, or not straight. May be used for straight vertical holes as alternative to topCoordinates and verticalHoleDepth  
*Type:* array, See special guidance on required format.  
*Condition:* Required if topCoordinate and verticalHoleDepth not used  
*Example:* ``[[525275.5,181543.2,15.25], [525275.5,181543.2,-9.75]]``

#### type
Type of exploratory hole, e.g. cable percussion borehole. May be code from standard list, or descriptive  
*Type:* string, Text (standard term)  
*Example:* ``CP+RC``

#### investigation
The investigation that this hole was undertaken for. May be simple text, or Reference to investigation object  
*Type:* string, Text, or reference to investigation object (part of top level structure not yet defined)  
*Example:* ``2019 GI Package A``

#### date
Date of exploration, normally taken as the start date for holes that take more thatn one day.   
*Type:* string, ISO 8601 date format  
*Example:* ``2019-05-23``

#### propertySetID
Link to additional property data for this hole. Could be used for additional hole metadata or for profiles of test results, e.g. SPT.  
*Type:* string, Reference to agsDataPropertySet propertySetID  
*Example:* ``PROP-A/BH01-SPT``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Original name on logs: BH1``

