
# Geometry

## Object model

### agsiGeometryExpHoleSet

An [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets,  e.g. sets for different investigations, or different types of hole. The data for the holes is included via the embedded [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) objects. Geologicical logging data is not included in these objects: see [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) and [agsiGeometryColumn](/Geometry_agsiGeometryColumn).

The parent object of [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) contains the following embedded child objects: 

- [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole)

[agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)

[agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``Holes-GI-A``

#### description
Short description of the set of holes defined here  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``2019 GI Package A``

#### agsiGeometryExpHole
See [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole)  
*Type:* array, [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) objects  


#### remarks
Additional remarks, if required  
*Type:* string, Text  


