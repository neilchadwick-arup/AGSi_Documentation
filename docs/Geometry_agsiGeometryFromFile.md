
# Geometry

## Object model

### agsiGeometryFromFile

An [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object is a pointer to geometry data contained within an external file, such as a CAD or model file. This object also includes metadata describing the file being referenced

The parent object of [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)

[agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC-TOP``

#### description
Short description of geometry defined here  
*Type:* string, Text  
*Example:* ``Top of LC``

#### geometryType
Nature of geometry represented  
*Type:* string, Text (standard term)  
*Condition:* Recommended  
*Example:* ``Surface``

#### fileFormat
Format of the data, i.e. file type.   
*Type:* string, Text (standard term)  
*Condition:* Recommended  
*Example:* ``LANDXML``

#### fileFormatVersion
if required, additional version information for file format used.  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``LandXML-1.2``

#### fileURI
URI for the data file. May be asbolute or relative to the current AGSi file.  
*Type:* string, URI  
*Condition:* Required  
*Example:* ``geometry/geom-vol-lc-top.xml``

#### fileLayer
For CAD or model files, if applicable, the layer/level on which the required data is located. Use with caution as the ability to interogate only a specified layer/level may not be supported in all software.  
*Type:* string, Text  
*Example:* ``Ground-LC``

#### fileVersion
Version information for the referenced file.  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``Rev P2, issued 17/10/2018``

#### fileVersionInfo
Revision notes for this version of the referenced file.  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``Updated for GIR rev P2. Additional BH from 2018 GI included.``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

