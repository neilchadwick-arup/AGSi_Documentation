
# Geometry

## Object model

### agsiGeometryLayer

An [agsiGeometryLayer](/Geometry_agsiGeometryLayer) object defines an element as the volume between two horizontal planes at specified elevations.

The parent object of [agsiGeometryLayer](/Geometry_agsiGeometryLayer) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryLayer](/Geometry_agsiGeometryLayer) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)

[agsiGeometryLayer](/Geometry_agsiGeometryLayer) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``GEOM-DESIGN-LC``

#### description
Short description of geometry defined here  
*Type:* string, Text  
*Example:* ``Design stratigraphy: London Clay``

#### topElevation
Elevation of the top surface  
*Type:* number  
*Condition:* Required  
*Example:* ``6``

#### bottomElevation
Elevation of the bottom surface. Use is strongly recommended as lower boundary of element may be ambiguous otherwise, i.e. it would rely on rely on the presence of an element or boudary below.  
*Type:* number  
*Condition:* Recommended  
*Example:* ``-30``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

