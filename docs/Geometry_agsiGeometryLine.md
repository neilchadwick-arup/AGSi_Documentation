
# Geometry

## Object model

### agsiGeometryLine

An [agsiGeometryLine](/Geometry_agsiGeometryLine) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, t#he plane that the lines lies in will be determined by the referencing object. Lines must comprise straight segments. Curves are not supported.

The parent object of [agsiGeometryLine](/Geometry_agsiGeometryLine) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryLine](/Geometry_agsiGeometryLine) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)
- [agsiModelBoundary](/Model_agsiModelBoundary)

[agsiGeometryLine](/Geometry_agsiGeometryLine) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``MODELBOUNDARY``

#### description
Short description of geometry defined here  
*Type:* string, Text  
*Example:* ``Boundary for Geological Model: sitewide``

#### coordinates2D
Co-ordinates of a 2D line, as an ordered list of co-ordinate pairs (x,y). What the co-ordinates represent is determined by the referencing object. To represent a polygon, the first and last coordinate pairs must be identical.  
*Type:* array, See special guidance on required format.  
*Condition:* Required for 2D line  
*Example:* ``[ [525000,181000], [525000,183000], [532000,183000], [532000,181000], [525000,181000] ]``

#### coordinates3D
Co-ordinates of 3D line, as an ordered list of co-ordinate triples, i.e.[ Easting(X), Northing (Y), Elevation (Z) ]  
*Type:* array, See special guidance on required format.  
*Condition:* Required for 3D line  
*Example:* ``[ [525100,181500, 16.2], [525150,181525, 18.3], [525200,181530, 19.1]``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

