
# Geometry

## Object model

### agsiGeometryVolFromSurfaces

An [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object defines an element as the volume between top and bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects. The volume is should be considered not to exist where the top and bottom surfaces are coexistent, or the top lies below the bottom. Volumes that fold back on themselves cannot be represented by this method.

The parent object of [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) is [agsiGeometry](/Geometry_agsiGeometry)

[agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)
- [agsiModelBoundary](/Model_agsiModelBoundary)

[agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) has the following additional attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](/Model_agsiModel) objects as required. All identifiers used within the [agsiGeometry](/Geometry_agsiGeometry) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC``

#### description
Short description of geometry defined here  
*Type:* string, Text  
*Example:* ``London Clay``

#### topGeometryID
Identifies the geometry for top surface, generally llinking to an [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object.  
*Type:* string, Reference to geometryID for an object in the [agsiGeometry](/Geometry_agsiGeometry) group  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC-TOP``

#### bottomGeometryID
Identifies the geometry for bottom surface, normally linking to an [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) object. Use is strongly recommended as lower boundary of element may be ambiguous otherwise, i.e. it would rely on rely on the presence of an element or boudary below.  
*Type:* string, Reference to geometryID for an object in the [agsiGeometry](/Geometry_agsiGeometry) group  
*Condition:* Recommended  
*Example:* ``GEOM-VOL-LMG-TOP``

#### remarks
Additional remarks, if required  
*Type:* string, Text  
*Example:* ``Some remarks if required``

