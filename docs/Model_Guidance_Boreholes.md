# Model

## Guidance on usage - Models showing exploratory holes

It is common for models to incorporate representations of exploratory holes and their
geological interpretation, typically displayed as cylinders with
different segments coloured to represent different geology.
These representations may be considered to be 'supporting data'
to the model itself. One of the benefits is it provides a visual indication of the
data used to generate the model.

!!! Todo
    Add image?

The section discusses how exploratory holes can be handled within AGSi.

The term **hole** is used in this section in the interests of brevity to
represent any type of exploratory hole including boreholes, coreholes, pits, CPT etc.

### Modelling exploratory holes - principles

AGSi allows the holes to be modelled separately from their geology.
These hole objects can be used to indicate the location and extent of
the hole as well as hole metadata. How this information is represented
in the model will be determined by the software being used for viewing.
Modelling the holes themselves in this way is optional in AGSi
as the representation of the geology (discussed below) does not rely on it.
Users (modellers and specifiers) should determine whether holes are to be
represented using this method.

Holes may be grouped in to **sets** of exploratory holes. The division of
holes between sets may be determined by the user.  Examples include
different sets for holes from different investigations,
or for different types of hole. Alternatively there may be no division,
in which case all holes belong to the same set.
Sets are important for the implementation of the schema as a single
[model element](/General_Definitions#model-element)
will reference one set of data (described below).  

!!! Note
    It is possible for each hole to have its own set and there may be situations
    where this appropriate. However, it is not recommended for the general case.

To model the geology found within holes, the hole is divided up in to **segments**
which are each assigned some geological (or similar) information.
This will commonly include a classification, e.g. [geological unit](/General_Definitions#geological-unit), and possibly
a description.

The segments may also be grouped in to sets, with a single
[model element](/General_Definitions#model-element)
referencing one set. Most commonly the sets will be aligned to the
geological classification. However, if the holes have been divided up into
sets then it the sets will typically align with
the combination of geological classification and hole.

It will commonly be the case that the geological classification and description
will be the same as found in the factual data, i.e. same as on the borehole log.
However, this does not need to be the case. For example, the modeller may prefer
to include simplified (interpreted) descriptions where full descriptions are
long and detailed.

It is possible for individual test results to be reported. This may be done
by creating and referencing [agsiDataPropertySet](/Data_agsiDataPropertySet) objects,
generally from the hole object.
An example showing how SPT results may be reported is described below.   

!!! Note
    AGSi is not intended to replace or replicate the AGS format for transfer of factual
    data. By default, the objects discussed here are only intended to
    carry limited information, most likely a summary of significant data selected
    by the creator of the model.


### Modelling exploratory holes - use of schema

The exploratory hole representations are considered to be part of a [model](/General_Definitions#model).
A [model](/General_Definitions#model) is established by defining an [agsiModel](/Model_agsiModel) object, which will
contain general metadata for the model.

If the overall [model](/General_Definitions#model) incorporates, for example, a 3D volume model,
then separate [agsiModelSubset](/Model_agsiModelSubset) objects may be defined for the 3D volume model and the
exploratory holes. The [agsiModelSubset](/Model_agsiModelSubset) objects are embedded within the applicable
[agsiModel](/Model_agsiModel) object under the corresponding attribute.

!!! Note
    Use of subsets is not an AGSi requirement. Users and specifiers should determine
    whether subsets are to be used.

A number of [model elements](/General_Definitions#model-element) are then defined using [agsiModelElement](/Model_agsiModelElement) objects.

For the exploratory holes themselves, each
[model element](/General_Definitions#model-element)
will comprise a **set**
of holes defined using the [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) object. The use of sets is
further described above.

Each individual hole is described using and [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object.
This includes the geometrical data for the hole as well as some limited
metadata. If further data is required to be associated with the hole then
this may be achieved populating an [agsiDataPropertySet](/Data_agsiDataPropertySet)
object and referencing it via the *[propertySetID](/Geometry_agsiGeometryExpHole#propertysetid)* attribute.

For each set of holes, the relevant [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) objects are embedded,
as an array, within the *[agsiGeometryExpHole](/Geometry_agsiGeometryExpHoleSet#agsigeometryexphole)* attribute of the
corresponding [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) object .

For the **segments** the process is identical to the above but with each set of
segments defined using an [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) object and the individual
columns described using [agsiGeometryColumn](/Geometry_agsiGeometryColumn) objects, with the latter embedded
within the former as an array using *[agsiGeometryColumn](/Geometry_agsiGeometryColumnSet#agsigeometrycolumn)* attribute.

A simple example illustrating these concepts is given below.

### Modelling exploratory holes - example

In this simplified example, there are two boreholes with geology (three
geological unit segments in each). For one of the boreholes a profile
of SPT N values with depth is provided.

!!! Note
    In the following the JSON data is broken up in to several sections to allow
    commentary to be provided. The different sections join up to
    form the full valid JSON data. The sections taken individually may
    not be valid JSON data by themselves. A copy of the full valid data
    file can be found here.

!!! Todo
    Add link

This first section contains model metadata in the [agsiModel](/Model_agsiModel) object.
In practice this would be the same for the different subsets within a model.

    :::JSON
    {
        "agsiModel": [
            {
                "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
                "name": "Geological Model: sitewide",
                "description": "Sitewide geological model incorporating 2019 GI data ",
                "type": "Geological model",
                "category": "Observational",
                "domain": "Engineering geology",
                "inputDesc": "Input data as described in GIR",
                "modelMethodDesc": "3D model created in Leapfrog. See GIR for details.",
                "usageDesc": "Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR for details.",
                "documentID": [
                    "GIR_P2",
                    "GDR_P1"
                ],
                "remarks": "Some additional remarks",

The exploratory holes are defined as a model subset
(this is optional, but good practice). This is done using an
[agsiModelSubset](/Model_agsiModelSubset) object, which is embedded within the parent [agsiModel](/Model_agsiModel) object.

    :::JSON
                "agsiModelSubset": [
                    {
                        "subsetID": "GEOLEXPHOLES",
                        "description": "Exporaratory holes"
                    }
                ],

Now the critical part, the formation of the model elements as [agsiModelElement](/Model_agsiModelElement) objects,
which are embedded within [agsiModel](/Model_agsiModel). Here there are four objects as follows:

  - one set of exploratory holes, which contains both holes
  - set of segments for Made Ground (MG)
  - set of segments for Alluvium (RTD)
  - set of segments for Gotham City Clay (GCC)

Each model element references a geometry object via the *[geometryID](/Model_agsiModelElement#geometryid)*
attribute. These are defined later.

Note also the references to the subset defined earlier via the *[subsetID](/Model_agsiModelElement#subsetid)* attribute.

    :::JSON
              "agsiModelElement": [
                  {
                      "elementID": "ExpHole-A",
                      "description": "Exp Holes - Package A",
                      "type": "Exploratory Hole",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Set",
                      "geometryID": "Holes-GI-A"
                  },
                  {
                      "elementID": "GeolCol-A-MG",
                      "description": "Exp Hole Geology - Made Ground",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-MG"
                  },
                  {
                      "elementID": "GeolCol-A-RTD",
                      "description": "Exp Hole Geology - River Terrace Deposits",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-ALV"
                  },
                  {
                      "elementID": "GeolCol-A-LC",
                      "description": "Exp Hole Geology - London Clay",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-GCC"
                  }
              ]
          }
      ],

The [agsiModel](/Model_agsiModel) object is now complete. We now define an [agsiGeometry](/Geometry_agsiGeometry) object
as a container for the geometry objects that follow.

    :::JSON

        "agsiGeometry": {
            "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
            "description": "Geometry data for boreholes",

We now define the geometry object for the set of holes referred to by the
relevant model element above. There could be an array of objects but
here there is just the one.

Embedded with the [agsiGeometryExpHoleSet](/Geometry_agsiGeometryExpHoleSet) object are two [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole)
objects for the two boreholes. Each has data for top coordinates, depth, type
and some other metadata.  

In addition, hole A/BH01 references a property set  (agsiDataPropertySet object)
via the *[propertySetID](/Geometry_agsiGeometryExpHole#propertysetid)* attribute. This set is defined later.  

    :::JSON

            "agsiGeometryExpHoleSet": [
                {
                    "geometryID": "Holes-GI-A",
                    "description": "2019 GI Package A",
                    "agsiGeometryExpHole": [
                        {
                            "expHoleID": "A/BH01",
                            "name": "BH01",
                            "topCoordinates": [525275.5,181543.2,15.25],
                            "verticalHoleDepth": 25,
                            "type": "CP",
                            "investigation": "2019 GI Package A",
                            "date": "2019-05-23",
                            "propertySetID": "PROP-A-BH01-SPT"
                        },
                        {
                            "expHoleID": "A/BH02",
                            "name": "BH02",
                            "topCoordinates": [525220.0,181550.0,17.2],
                            "verticalHoleDepth": 50,
                            "type": "CP+RC",
                            "investigation": "2020 GI Package A",
                            "date": "2019-05-27"
                        }
                    ]
                }
            ],

Now for the geology column segments, for which we have an array of three
[agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) objects for MG, ALV and GCC respectively.

In this example each [agsiGeometryColumnSet](/Geometry_agsiGeometryColumnSet) contains two
[agsiGeometryColumn](/Geometry_agsiGeometryColumn) objects, one for each of the exploratory holes in the set.
The columns segments have attributes that define their top coordinates (xyz)
along with a bottom elevation. There are also attributes for description and other
data such as geology code.

The *[expHoleID](/Geometry_agsiGeometryColumn#expholeid)* attribute is a reference to the corresponding
[agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object. Such a linkage is optional, but good practice.

    :::JSON

            "agsiGeometryColumnSet": [
                {
                    "geometryID": "GEOLCOL-GI-A-MG",
                    "description": "Made Ground (2019 GI Package A)",
                    "columnType": "Geology",
                    "agsiGeometryColumn": [
                        {
                            "topCoordinates": [525275.5,181543.2,15.25],
                            "vertBottomElevation": 13.5,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Very soft and soft brown, dark brown and grey slightly sandy slightly gravelly CLAY",
                            "legendCode": "102",
                            "geologyCode": "MG"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,17.2],
                            "vertBottomElevation": 14.1,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Brown slightly clayey SAND with some subangular to subrounded fine to coarse fravel of flint, concrete and brick. Occasional subangular cobbles of concrete. ",
                            "legendCode": "102",
                            "geologyCode": "MG"
                        },
                        {
                            "topCoordinates": [525275.5,181543.2,13.5],
                            "vertBottomElevation": 8.9,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Soft brown silty sandy  CLAY",
                            "legendCode": 207,
                            "geologyCode": "ALV"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,14.1],
                            "vertBottomElevation": 9.4,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Vert soft/loose brown sandy SILT",
                            "legendCode": "303",
                            "geologyCode": "ALV"
                        },
                        {
                            "topCoordinates": [525275.5,181543.2,8.9],
                            "vertBottomElevation": 13.5,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (<0.1m thick)",
                            "legendCode": "202",
                            "geologyCode": "GCC"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,9.4],
                            "vertBottomElevation": 14.1,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Stiff to very stiff blue grey silty CLAY",
                            "legendCode": "202",
                            "geologyCode": "GCC"
                        }
                    ]
                }
            ]
        },

Finally (although the order of the data is not important) we have some SPT N data
(for one of the boreholes only, for brevity).
This is the data referenced by the [agsiGeometryExpHole](/Geometry_agsiGeometryExpHole) object for A/BH01.
The reference is to the ID of the [agsiDataPropertySet](/Data_agsiDataPropertySet) object.
This object contains a single
[agsiDataPropertyValue](/Data_agsiDataPropertyValue) object with all of the results for that borehole.
The individual results are given as a list of elevation and N value pairs,
using the *[valueProfileCoordinates](/Data_agsiDataPropertyValue#valueprofilecoordinates)* attribute.

All data objects are incorporated within a single [agsiData](/Data_agsiData) container.

    :::JSON

        "agsiData": {
            "dataSetID": "40b785d0-547c-41c5-9b36-3cd31126ecb0",
            "description": "Example Project interpreted data",
            "codeDictionary": "https://gitlab.com/AGS-DFWG-Web/ASGi/agsCodeList.htm",
            "agsiDataCode": [
                {
                    "codeID": "SPTN",
                    "description": "SPT N value",
                    "units": "Blows/300mm"
                }
            ],
            "agsiDataPropertySet": [
                {
                    "propertySetID": "PROP-A-BH01-SPT",
                    "description": "SPT N for A/BH01",
                    "remarks": "Only the top few data points shown for this example",
                    "agsiDataPropertyValue": [
                        {
                            "codeID": "SPTN",
                            "valueProfileIndVarCodeID": "ELEV",
                            "valueProfileCoordinates": [
                                [14.0,21.0],
                                [13.0,8.0],
                                [12.0,7.0],
                                [11.0,27.0],
                                [10.0,38.0],
                                [8.5,22.0]
                            ]
                        }
                    ]
                }
            ]
        }
    }
