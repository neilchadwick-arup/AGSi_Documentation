# Model

## Guidance on usage: Overview
S
### What is a model?

A **model** is a digital geometric (2D or 3D) representation of the ground.

In theory, an AGSi model can be whatever the user wants it to be.
AGSi merely seeks to provide a data transfer format that will allow users to
exchange any type of model, within reason, albeit preferably ground related.

In particular, AGSi can support models for different:

* [domains](/General_Definitions#domain-of-model) (e.g. geology, hydrogeology, geotechnical)
* [categories of model ](/General_Definitions#category-of-model))(e.g.
  [observational](/General_Definitions#observational-model) or
  [analytical](/General_Definitions#analytical-model))
* forms of geometrical representation (e.g. 3D volumes, 2D sections, borehole
  columns)

  Note: [model](/General_Definitions#model),
  [domain](/General_Definitions#domain-of-model),
  [category](/General_Definitions#category-of-model) and
  [type ](/General_Definitions#type-of-model)are defined terms in an AGSi
  model context. Refer to the Definitions page for full details.

!!! Todo
    add links to definitions page

In order to provide a sensible structure to the AGSi schema some
assumptions have been made about how models are likely to be formed and used.
The AGSi schema adopted is based on these assumptions. Recommendations on how
to implement the schema for typical modelling scenarios are provided in the
following. It is hoped that the schema and the recommendations for implementation
will satisfy the majority of users (specifiers, modelers, model users and
software developers).

Alternative implementations of the schema may be possible.
However, users should be aware that non standard implementations may cause
confusion and may be problematic for some software.

!!! Note
    AGS welcomes feedback on the schema and the recommended implementations.
    The schema is based on a few examples of current practice with some thought
    given as to how this may develop in the near future.
    However, it is recognised that this is a developing subject and future practice
    may evolve in different ways.

### AGSi recommended implementation - Model

The AGSi recommendations regarding what constitutes a model are illustrated in the
diagram below, with commentary provided below.

  Todo: add diagram

Each of the blue boxes represent a separate model. Models 1, 2 and 3 are
different [types](/General_Definitions#types) of model, as follows:

1. Geological model: an [observational model](/General_Definitions#observational-model), showing interpreted 'best guess'
geometry of the geological units. Use case typically restricted to visualisation
only. This model incorporates a number of different subsets which are further
discussed in the next section.

2. Hydrogeological model: may be similar to the geological model, but optimised
for hydrogeological modelling. This may be an [analytical model](/General_Definitions#analytical-model)
intended for direct import to analysis software.

3. Geotechnical design model: an [analytical model](/General_Definitions#analytical-model) intended to be used for
analysis/design and often limited to a specific use case
([specified purpose](/General_Definitions#specified-purpose-of-model-or-data)),
e.g. limited to foundation design in the example shown.
Normally based on interpretation and simplification of the geological
(observational) model, taking account of uncertainty and the needs of the
specified use case.

Model 4 is another geotechnical design model, but for a different use case.
In this example the geometries of models 3 and 4 are different.
If the geometries were the same and the only difference between the models
was the parameters referenced, then it would be possible to use a single model
for both by utilising the 'case' concept incorporated in the Data group.
For more details of how to do this see [agsiData - Use of case](/Data_Guidance#Use-of-Case).


### AGSi recommended implementation - Model subset

Model 1 is an example of how different types of information, or different forms
of geometric representation, can be incorporated into a model.
This particular example includes:

(a) 3D volume model of geological units
(b) Columns representing the geology observed in boreholes
(c) Cross sections shown within the volume model

!!! Todo
    Check format of the above is ok

In this case (a) would normally be considered to be the primary view of the
model, with (b) and (c) providing supporting or clarifying information.

The schema allows for each of the above to be **optionally** identified as
**subsets** of the model, using the [agsiModelSubset](/Model_agsiModelSubset) object.
For each subset an [agsiModelSubset](/Model_agsiModelSubset) object is created with an identifier.
Model elements (agsiModelElement objects) can be tagged to a particular
subset by referencing the identifier in the
*[subsetID](/Model_agsiModelElement#subsetid)* attribute.

!!! Todo
    add link to further info?

The above describes a capability built in to AGSi.
However, it is possible that subsets may not be supported by all software.
This should be considered and taken into account by specifiers and modelers.

In theory, it would be possible to incorporate the different models 1 to 4
above into the same model, and define these as model subsets.
However, this is not recommended.

!!! Note
    The preference is to keep models simple, preferably representing a
    single domain, category and a clear single use case.
    Subsets may be used to identify different geometric representations, or
    supporting information.
    There may be several AGSi models on a project,
    but these may be combined into the same data set (file).


### Model boundaries

It is possible that the geometry defined for a model may also fully
define the true extent of the model.
However, this is commonly not the case, e.g. a surface or volume may
extend beyond what is considered to be the limit of validity of the model.

The [agsiModelBoundary](/Model_agsiModelBoundary) object may be used to establish a simple boundary for
the model. The boundaries that can be defined are:

* plan area boundary, assumed to have vertical sides ('cookie-cutter')
* bottom of model boundary as a horizontal plane or a surface

The model is  defined as the volume enclosed within the plan area boundary
and above the bottom boundary.

!!! Note
    The [agsiModelBoundary](/Model_agsiModelBoundary) object can only be used for 3D models.

[agsiModelBoundary](/Model_agsiModelBoundary) allows two different methods of defining the plan area
boundary:

1. Simple rectangular box defined by limiting coordinates
2. Closed polygon (requires reference to an [agsiGeometry](/Geometry_agsiGeometry) object)

and two different methods of defining the bottom boundary:

1. Horizontal plane at a specified elevation
2. Surface (requires reference to an [agsiGeometry](/Geometry_agsiGeometry) object)

!!! Note
    There is no support for a top boundary.
    It is assumed that the top of the model will be clearly defined by
    an appropriate model element, e.g. the ground surface, or top of topmost
    layer.
