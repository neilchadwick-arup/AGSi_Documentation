# Model

## Guidance on usage: Volume models


3D volume models are expected to be one of the most common applications of AGSi.
This includes 3D geological models and geotechnical design models
as well as similar models in other domains. Examples of how to form these
are provided below.


### Geological model using surfaces - principles

A [geological model](/General_Definitions#geological-model), more correctly defined as an
[engineering geology observational model](/General_Definitions#engineering-geology-observational-model)
, normally comprises volumes that each
represent a geological unit. The extents of the units are normally derived from
interpolation between known observation points such as boreholes.

These units are often defined using surfaces from which the volumes
associated with the units are implied. AGSi supports this method, as
described here. The alternative of using true volume geometry, which is
more straightforward, is described below.

Surfaces describing the top and/or bottom of units may be curved, e.g. derived
using surface fitting methods, or formed of adjoining planar surfaces,
e.g. triangular irregular network (TIN) surfaces. Different software packages
may use different methods.

For AGSi it is strongly recommended that both top and bottom surfaces are
defined for each unit. This should remove any ambiguity regarding the extent
of the unit in question.

!!! Note
    If only one surface (top or bottom) is defined it is assumed that
    the unit extends, up or down as applicable, to the next surface.
    In such cases there is potential for error if the top/bottom surface for
    an intermediate unit is incorrectly omitted from the model.
    Such errors may not be obvious and may remain undetected.
    If both top and bottom are included then all
    units should be defined correctly. In this case, if a unit is omitted
    there would be a visible gap in the model.

The surfaces that define the top and bottom of the model must be single
continuous surfaces.
For simple layered models the bottom of one unit may be the same as the top of
another. In such cases it is possible to define one surface that can be used
for both.
However, in cases where some layers are intermittent, the bottom of one unit may
interface with more than one underlying unit. In such cases it will be
necessary to construct a separate bottom of unit surface.

The treatment of intermittent layers will require careful consideration by
the modeller. Two possible approaches are illustrated in the diagram below:

!!! Todo
    add image

* Single [model element](/General_Definitions#model-element) with continuous top and bottom surfaces across the model,
  but these are coincident in areas where the unit is absent
* Separate [model_elements](/General_Definitions#model-element) for each area where the unit is present, but the
  geometry for each is curtailed to the area where the unit exists

AGSi can support both methods, and there is no preference.
However, specifiers and modellers should consider software compatibility
when selecting the method to be used.
Mixing of methods within a model is not recommended.

!!! Note
    Curtailing the extent of an element in plan can be achieved easily
    in AGSi by implementing the *[elementBoundaryGeometryID](/Model_agsiModelElementBoundary#elementboundarygeometryid)*
    attribute of [agsiModelElement](/Model_agsiModelElement).
    However, this should be used with caution as it may not be
    supported by all software.

Sometimes the boreholes themselves are included in the model.
For guidance on how to achieve this see
[Guidance on usage - Boreholes](/Model_Guidance_Boreholes)


### Geological model using surfaces - use of schema

A single [model](/General_Definitions#model) is established by defining an [agsiModel](/Model_agsiModel) object, which will
contain general metadata for the model.

If the overall model will incorporate other representations or supporting data,
e.g. borehole sticks, then [agsiModelSubset](/Model_agsiModelSubset) objects may be defined for each of these.

A [model boundary](/General_Definitions#model-boundary) may also be defined using an [agsiModelBoundary](/Model_agsiModelBoundary) object.

The [agsiModelSubset](/Model_agsiModelSubset) and
[agsiModelBoundary](/Model_agsiModelBoundary)
objects are embedded within the applicable
[agsiModel](/Model_agsiModel) object under the corresponding attributes.

A number of [model elements](/General_Definitions#model-element) elements are then defined using [agsiModelElement](/Model_agsiModelElement) objects.
Typically, each geological unit will be one
[model element](/General_Definitions#model-element),
although as discussed
above this may not always be the case, especially with units that are
not continuous. All of the [model elements](/General_Definitions#model-element)
are embedded, as an array, within
the [agsiModel](/Model_agsiModel) object.

If subsets are used the *[subsetID](/Model_agsiModelElement#subsetid)*
attribute should contain the identifier
for the relevant [agsiModelSubset](/Model_agsiModelSubset) object.  [Model_elements](/General_Definitions#model-element) may be similarly
linked to data or features by including the identifiers to the
relevant objects in those groups under the corresponding attributes.

Each [agsiModelElement](/Model_agsiModelElement) has an attribute ***geometryID*** which contains the identifier
for the geometry object that defines the geometry for that element.
For the geological units in this example this geometry object will be a
[agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object.

For a geological model the surfaces themselves will normally
be defined by reference to surface geometry stored
in external files, e.g. DWG, DGN, IFC or LandXML. This definition is provided by
[agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects which identify the type and location of the file.

!!! Note
    References to files in AGSi use the URI protocol as defined in ... See ...
    The location of the external file relative to the AGSi file will be important if a relative link is used.

!!! Todo
    add citation and link

If AGSi recommendations are followed both top and bottom surfaces will be defined
for each unit using a single [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object.
The [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object has *[topGeometryID](/Geometry_agsiGeometryVolFromSurfaces#topgeometryid)* and *[bottomGeometryID](/Geometry_agsiGeometryVolFromSurfaces#bottomgeometryid)*
attributes that will contain the identifiers for the relevant
[agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects for the applicable surfaces.

All geometry objects are contained with the parent [agsiGeometry](/Geometry_agsiGeometry) object.

### Geological model using surfaces - example

An example of a very simple geological model illustrating the above concepts
is given below.

!!! Note
    In the following the JSON data is broken up in to several sections to allow
    commentary to be provided. The different sections join up to
    form the full valid JSON data. The sections taken individually may
    not be valid JSON data by themselves. A copy of the full valid data
    file can be found here.

!!! Todo
    Add link

This first section contains model metadata in the [agsiModel](/Model_agsiModel) object.
In practice this would be the same for the different subsets within a model.

    :::JSON
    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geological model",
              "type": "Geological model",
              "category": "Observational",
              "domain": "Engineering geology",
              "usageDesc": "Visualisation only",

This next part establishes the model boundary using an [agsiModelBoundary](/Model_agsiModelBoundary)
object. In this case the boundary is a simple
rectangular box defined by limiting coordinates and a base elevation.

    :::JSON
              "agsiModelBoundary": {
                  "minEasting": 12000,
                  "maxEasting": 13000,
                  "minNorthing": 5000,
                  "maxNorthing": 6000,
                  "baseElevation": -25
              },

The volume model is defined as a model subset
(this is optional, but good practice). This is done using an
[agsiModelSubset](/Model_agsiModelSubset) object, which is embedded within the parent [agsiModel](/Model_agsiModel) object.

    :::JSON

              "agsiModelSubset": [
                  {
                      "subsetID": "GEOL-VOL",
                      "description": "Geological unit volumes (from surfaces)"
                  }
              ],

Now for the elements that make up the model, in this case three geological units.
Each is defined by an [agsiModelElement](/Model_agsiModelElement) object. The geometry for each is
identified by reference to [agsiGeometry](/Geometry_agsiGeometry) objects, defined later, using the *[geometryID](/Model_agsiModelElement#geometryid)* attribute.

    :::JSON

              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-GCC"
                  }
              ]
          }
      ],

The [agsiModel](/Model_agsiModel) object is now complete. We now define an [agsiGeometry](/Geometry_agsiGeometry) object
as a container for the geometry objects that follow.

    :::JSON

      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",

We now define the geometry objects for the geological units referred to by the
relevant model element above.

Each unit is defined as an [agsiGeometryVolFromSurfaces](/Geometry_agsiGeometryVolFromSurfaces) object which, in turn,
reference geometry objects for top and bottom surfaces
(defined in the next part).

    :::JSON

          "agsiGeometryVolFromSurfaces": [
              {
                  "geometryID": "Geol-MG",
                  "topGeometryID": "Surface-Geol-MG",
                  "bottomGeometryID": "Surface-Geol-ALV"
              },
              {
                  "geometryID": "Geol-ALV",
                  "topGeometryID": "Surface-Geol-ALV",
                  "bottomGeometryID": "Surface-Geol-GCC"
              },
              {
                  "geometryID": "Geol-GCC",
                  "topGeometryID": "Surface-Geol-GCC",
                  "bottomGeometryID": "Model-base"
              }
          ],

The geometry for the surfaces themselves is provided in external files.
Therefore we use [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile) objects to point to this data.
Note that some surface are referenced twice, ie. bottom of one layer and
top of another.

An exception here is the final agsiGeometryPlane object which is the bottom
surface of the lowest layer for the purposes of this model, ie. bottom of model.

    :::JSON

          "agsiGeometryFromFile": [
              {
                  "geometryID": "Surface-Geol-MG",
                  "description": "Top of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-MG-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-ALV",
                  "description": "Top of ALV / Bottom of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-ALV-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-GCC",
                  "description": "Top of GCC / Bottom of ALV",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-GCC-top.xml"
              }
          ],
          "agsiGeometryPlane": [
              {
                  "geometryID": "Model-base",
                  "elevation": -25
              }
          ]
      }
    }

### Geological volume using volume geometry

!!! Note
    It is anticipated that use of true volume geometry for ground
    models will become more commonplace in the future.

The process for using 'true' volume geometry for the model instead of
surfaces with implied geometry is almost the same, but more straigthforward.

The volume geometry for each unit will be defined by an [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile)
object, which will point to a file with geometry in a suitable format.

There will be only one volume geometry object for each
[model elements](/General_Definitions#model-element) and
therefore the
[agsiGeometryFromFile](/Geometry_agsiGeometryFromFile)
object should directly reference the
identifier for this object in the *[geometryID](/Geometry_agsiGeometryFromFile#geometryid)* attribute.


### Simple geotechnical design model using layers

Simple geotechnical design models that have horizontal layers,
such as the example below, can be defined very easily.
No external reference files are required.

!!! Todo
    add image

The general process using [agsiModel](/Model_agsiModel) and [agsiModelElement](/Model_agsiModelElement) is as described above.

For the geometry of each geotechnical unit (layer) the [agsiGeometryLayer](/Geometry_agsiGeometryLayer)
object can be used. This has attributes for
*[topElevation](/Geometry_agsiGeometryLayer#topelevation)* and *[bottomElevation](/Geometry_agsiGeometryLayer#bottomelevation)*, both of which must both be defined.
This identifier for this geometry object is then directly referenced by the
*[geometryID](/Model_agsiModelElement#geometryid)* attribute in the relevant [agsiModelElement](/Model_agsiModelElement) object.

It is also possible to use the *[elementBoundaryGeometryID](/Model_agsiModelElementBoundary#elementboundarygeometryid)* attribute to
define a limiting valid area for each
[model element](/General_Definitions#model-element).
The bounding polygon itself would be defined as
an [agsiGeometryLine](/Geometry_agsiGeometryLine) object referenced by the *[elementBoundaryGeometryID](/Model_agsiModelElementBoundary#elementboundarygeometryid)*
attribute in agsModelElement.

Layers with sloping surfaces cannot be modelled using this method.
For these it will necessary to use one of the methods described
above for the geological model.

!!! Note
    Consideration has been given to including a geometry
    object that could model simple sloping surfaces.
    Feedback on this subject would be welcome.

Some example data illustrating the above is given below. This example
has three horizontal layers.  

!!! Note
    Limiting valid areas and links to data/features
    and some other useful attributes that would normally benefit
    such a model have been excluded for brevity.
    A copy of the full data file is included here.

!!! Todo
    Add link

    :::JSON
    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geotechnical design model",
              "type": "Geotechnical design",
              "category": "Analytical",
              "domain": "Geotechnical",
              "usageDesc": "Design for some specified use case",
              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-GCC"
                  }
              ]
          }
      ],
      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
          "agsiGeometryLayer": [
              {
                  "geometryID": "MG",
                  "topElevation": 22.5,
                  "bottomElevation": 19
              },
              {
                  "geometryID": "ALV",
                  "topElevation": 19,
                  "bottomElevation": 14.5
              },
              {
                  "geometryID": "GGC",
                  "topElevation": 14.5,
                  "bottomElevation": -20
              }
          ]
      }
    }
