# Model

## Introduction

### Overview

In an AGSi context, a [model](/General_Definitions#model)
is a digital geometric (2D or 3D) representation of the ground.

The Model group (agsiModel) is used to define and assemble geometry based models.
These models are built up from elements which are linked to geometry objects
stored in the Geometry (agsiGeometry) group.
Model elements may also be linked to data stored in the Data (agsiData) group.

There may be more than one model defined in an AGSi dataset (file).

For further information on what is considered to be a single model in an AGSi context,
which includes guidance on how models can be divided up into subsets,
please refer to [Guidance on usage, Model - Overview](/Model_Guidance_General/#Model---Overview).

!!! TODO
    check above works - spaces to dashes!

The Feature group (agsiFeature) is closely related to Model, but subtly different.
This is further discussed in ...

!!! TODO
    where to discuss this?

### Summary of schema

This diagram shows the objects in the Model group and their relationships.

![Model summary UML diagram](images/agsiModel_summary_UML.png)

A single model is defined by an [agsiModel](/Model_agsiModel) object.
There may be several  objects in a data set (file).
Each [agsiModel](/Model_agsiModel) object contains objects from the classes described below.

The basic building blocks of each model are model elements.
The model elements are defined as [agsiModelElement](/Model_agsiModelElement) objects.
These elements are then collected together within the appropriate [agsiModel](/Model_agsiModel) object,
i.e. [agsiModelElement](/Model_agsiModelElement) objects are embedded within the parent [agsiModel](/Model_agsiModel) object.

In addition, a model boundary may also be specified using an [agsiModelBoundary](/Model_agsiModelBoundary)
object. Each model may contain only one [agsiModelBoundary](/Model_agsiModelBoundary), which will be embedded within the parent [agsiModel](/Model_agsiModel) object.

Each model may optionally be divided up into model subsets.
These subsets are defined using the [agsiModelSubset](/Model_agsiModelSubset) object, which will be embedded within the parent [agsiModel](/Model_agsiModel) object.
Subsets are then created by referencing the relevant [agsiModelSubset](/Model_agsiModelSubset)
identifier in the *[subsetID](/Model_agsiModelElement#subsetid)*
 attribute of each
[agsiModelElement](/Model_agsiModelElement) object.

Each model element can belong to no more than one subset.

!!! Note
    Use of subsets is optional. Some software may not be able to
    support subsets and therefore its use should be carefully considered
    by specifiers and modellers.

### Schema UML diagram

This diagram shows the full schema of the Model group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Model full UML diagram](images/agsiModel_full_UML.png)
