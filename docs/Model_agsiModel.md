
# Model

## Object model

### agsiModel

An [agsiModel](/Model_agsiModel) object is the parent object for a single model. There may be several [agsiModel](/Model_agsiModel) objects in an AGSi dataset.  However, a single [agsiModel](/Model_agsiModel) object may itself incorporate different ways of representing the model information and supporting data, e.g. 3D volumes, cross sections and exploratory hole logs may all be incorporated into the same [agsiModel](/Model_agsiModel). These different representations may be (optionally) identified as subsets of the model using [agsiModelSubset](/Model_agsiModelSubset) objects. 

The parent object of [agsiModel](/Model_agsiModel) is root

[agsiModel](/Model_agsiModel) contains the following embedded child objects: 

- [agsiModelElement](/Model_agsiModelElement)
- [agsiModelBoundary](/Model_agsiModelBoundary)
- [agsiModelSubset](/Model_agsiModelSubset)

[agsiModel](/Model_agsiModel) has the following additional attributes:


#### modelID
Identifier, preferably UUID, for each model.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

#### name
Short name of model  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``Geological Model: sitewide``

#### description
More verbose description of model, if required.  
*Type:* string, Text  
*Example:* ``Sitewide geological model incorporating 2019 GI data ``

#### type
Type of model. Use of IDBE terminology recommended. Incorporates domain and category.  
*Type:* string, Text (standard term)  
*Condition:* Recommended  
*Example:* ``Geological model``

#### category
Domain of model . Use of IDBE terminology recommended.   
*Type:* string, Text (standard term)  
*Example:* ``Observational``

#### domain
Category of model (see definitions)  
*Type:* string, Text (standard term)  
*Example:* ``Engineering geology``

#### inputDesc
Short description of input data used by model  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``Input data as described in GIR``

#### modelMethodDesc
Short description of method used to create model  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``3D model created in Leapfrog. See GIR for details.``

#### usageDesc
Short description of usage limitations or restrictions  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR for details.``

#### documentID
Reference to documentation relating to model  
*Type:* array, Reference to agsDocument objects  
*Example:* ``["GIR_P2","GDR_P1"]``

#### agsiModelElement
See [agsiModelElement](/Model_agsiModelElement)  
*Type:* array, [agsiModelElement](/Model_agsiModelElement) objects  


#### agsiModelBoundary
See [agsiModelBoundary](/Model_agsiModelBoundary)  
*Type:* object, [agsiModelBoundary](/Model_agsiModelBoundary) object  


#### agsiModelSubset
See [agsiModelSubset](/Model_agsiModelSubset)  
*Type:* object, [agsiModelSubset](/Model_agsiModelSubset) objects  


#### remarks
Additional remarks, if required.  
*Type:* string, Text  
*Example:* ``Some additional remarks``

