
# Model

## Object model

### agsiModelBoundary

An [agsiModelBoundary](/Model_agsiModelBoundary) object defines the model boundary, i.e the maximum extent of the model. Any elements or parts of elements lying outside the boundary are deemed to be not part of the model. Only one boundary per [agsiModel](/Model_agsiModel) is permitted. Only plan boundaries with vertical sides are permitted, defined by either limiting coordiantes, or a polygon. The base may be either a flat plane at a defined elevation, or a surface.

The parent object of [agsiModelBoundary](/Model_agsiModelBoundary) is [agsiModel](/Model_agsiModel)

[agsiModelBoundary](/Model_agsiModelBoundary) has the following additional attributes:


#### description
Short description.  
*Type:* string, Text  
*Example:* ``Boundary for Geological Model: sitewide``

#### minEasting
Minimum Easting (X axis) for box boundary  
*Type:* number  
*Example:* ``525000``

#### maxEasting
Maximum Easting (X axis) for box boundary  
*Type:* number  
*Example:* ``532000``

#### minNorthing
Minimum Northing (Y axis) for box boundary  
*Type:* number  
*Example:* ``181000``

#### maxNorthing
Maximum Northing (Y axis) for box boundary  
*Type:* number  
*Example:* ``183000``

#### baseElevation
Elevation of basal plane of model for box boundary  
*Type:* number  
*Example:* ``-40``

#### boundingPolygon
Reference to polygon defining plan extent of model, as alternative to box boundary. May not be supported by all software.  
*Type:* string, Reference to [agsiGeometryLine](/Geometry_agsiGeometryLine) geometryID  
*Example:* ``MODELBOUNDARY``

#### baseSurface
Reference to surface defining base of model, as alternative to box boundary. May not be supported by all software.  
*Type:* string, Reference to [agsiGeometryFromFile](/Geometry_agsiGeometryFromFile)  
*Example:* ``MODELBASE``

#### remarks
Additional remarks, if required.  
*Type:* string, Text  
*Example:* ``Some additional remarks``

