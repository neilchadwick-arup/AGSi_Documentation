
# Model

## Object model

### agsiModelElement

A model is made up of elements. These elements defined by [agsiModelElement](/Model_agsiModelElement) objects. Each element will have geometry assigned to it, by reference to an object in the [agsiGeometry](/Geometry_agsiGeometry) Group. Which object is referenced will depend on tthe form of geometry required. Elements may optionally be associated with an agsiFeature object and they may also have data associated with them. See special guidance for examples of models and elements.

The parent object of [agsiModelElement](/Model_agsiModelElement) is [agsiModel](/Model_agsiModel)

[agsiModelElement](/Model_agsiModelElement) has associations (reference links) with the following objects: 

- agsiFeature
- [agsiModelSubset](/Model_agsiModelSubset)
- [agsiDataPropertySet](/Data_agsiDataPropertySet)
- [agsiDataParameterSet](/Data_agsiDataParameterSet)
- any of the objects in the [agsiGeometry](/Geometry_agsiGeometry) group

[agsiModelElement](/Model_agsiModelElement) has the following additional attributes:


#### elementID
Unique identifier for element. Must be unique within model.  
*Type:* string, Identifier  
*Condition:* Required  
*Example:* ``LC-W``

#### description
Name or short description.  
*Type:* string, Text  
*Condition:* Recommended  
*Example:* ``London Clay, west zone``

#### type
Type of element  
*Type:* string, Text (standard term)  
*Condition:* Recommended  
*Example:* ``Geological unit``

#### featureID
Link to agsFeature relevant to this element.  
*Type:* string, Reference to agsiFeature featureID  
*Example:* ``LC``

#### subsetID
Idenfities the subsets that this element belongs to, if applicable.  Link to subsetID in [agsiModelSubset](/Model_agsiModelSubset).   
*Type:* string, Reference to [agsiModelSubset](/Model_agsiModelSubset) subsetID  
*Example:* ``GEOLUNITVOLUMES``

#### geometryForm
Geometric form of element  
*Type:* string, Text (standard term)  
*Condition:* Recommended  
*Example:* ``Volume from surfaces``

#### geometryID
Identifies the geometry for this element. The object type referenced will depend on the type of geometry, which should be compatible with that defined in geometryType.  
*Type:* string, Reference to geometryID for an object in the [agsiGeometry](/Geometry_agsiGeometry) group  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC``

#### areaLimitGeometryID
If required, a limiting plan area for the element may be defined by reference to a polygon object. The polygon acts as a 'cookie cutter' so the element boundary will be curtailed to stay within the polygon. Geometry beyond the boundary is ignored. This allows a large element to be easily divided up into parts, e.g. to allow different properties or parameters to be reported for each part.  
*Type:* string, Reference to geometryID for an object in the [agsiGeometry](/Geometry_agsiGeometry) group  
*Example:* ``GEOM-AREA-LC-W``

#### propertySetID
Link to property data for object  
*Type:* string, Reference to agsDataPropertySet propertySetID  
*Example:* ``PROP-LC-W``

#### parameterSetID
Link to parameter data for object  
*Type:* string, Reference to agsDataParameterSet parameterSetID  
*Example:* ``PARAM-LC-W``

#### colourRGB
Recommended display colour (RGB hexadecimal)  
*Type:* string, RGB hex colour  
*Example:* ``#c0c0c0``

#### remarks
Additional remarks, if required.  
*Type:* string, Text  
*Example:* ``Some additional remarks``

