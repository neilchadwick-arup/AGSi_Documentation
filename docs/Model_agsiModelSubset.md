
# Model

## Object model

### agsiModelSubset

[agsiModelSubset](/Model_agsiModelSubset) identify optional subsets of the model. Subsets are created by reference to these objects from [agsiModelElement](/Model_agsiModelElement). How subsets are used may be determined by the author of the model. See special guidance for examples of how subsets may be used. 

The parent object of [agsiModelSubset](/Model_agsiModelSubset) is [agsiModel](/Model_agsiModel)

[agsiModelSubset](/Model_agsiModelSubset) has associations (reference links) with the following objects: 

- [agsiModelElement](/Model_agsiModelElement)

[agsiModelSubset](/Model_agsiModelSubset) has the following additional attributes:


#### subsetID
Identifier referenced by [agsiModelElement](/Model_agsiModelElement)  
*Type:* string  
*Example:* ``GEOLUNITVOLUMES``

#### description
Short description.  
*Type:* string, Text  
*Example:* ``Geological unit volumes (from surfaces)``

#### remarks
Additional remarks, if required.  
*Type:* string, Text  
*Example:* ``Some additional remarks``

