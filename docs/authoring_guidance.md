
# AGS documentation authoring guide

The AGSi documentation uses the following technologies to create a browser-based, indexed user-guide with software-level version control:

- Python 3.6+
    - MkDocs (Markdown)
        - material for MkDocs
    - plantuml
    - blockdiag


- Git version control
    - GitLab online repository
        - GitLab Pages

MkDocs is a Python library that translates markdown text files into html pages. The AGSi documentation uses the "material" theme to style the pages. Diagrams are created using Plantuml or blockdiag syntax, which uses a text input to create a diagram on the fly.

The GitLab online repository allows direct editing online, however the preferred method of document development is to author offline and push the changes to the GitLab online repository.

Publishing to the web is carried out automatically using a pipeline command.

Authoring offline is carried out using Markdown in a text editor and previewed using a local server, which is part of the MkDocs framework.

All key software used is open source, free and cross-platform.

## Requirements:

Online:

1. [GitLab](https://gitlab.com) account and access to the
2. [repository](https://gitlab.com/ags-data-format-wg/AGSi_Documentation).
3. The documentation is available to the public here: [https://ags-data-format-wg.gitlab.io/AGSi_Documentation](https://ags-data-format-wg.gitlab.io/AGSi_Documentation )

```
https://gitlab.com/ags-data-format-wg/AGSi_Documentation
```

Desktop:

1. [Python 3.6+](https://www.python.org)
2. [MkDocs](https://www.mkdocs.org) (install via pip)
3. [material](https://squidfunk.github.io/mkdocs-material/) (markdown style extension install via pip)
4. [include](https://github.com/cmacmackin/markdown-include) (markdown extension to include/reference files)
5. [blockdiag](http://blockdiag.com/en/blockdiag/index.html) For simple relationship charts
6. [plantuml](https://pypi.org/project/plantuml/) (for UML diagrams) (install via pip)

!!! note
    Python can be installed from the Python.org web site, or the [Anaconda](https://www.anaconda.com) suite can be used. Note that Anaconda is a large download and install.

```
pip install mkdocs
pip install mkdocs-material
pip install markdown-blockdiag
pip install markdown-include
pip install plantuml-markdown
```

Optional software:

1. [git for Windows GUI](https://gitforwindows.org) for Windows only

```
https://gitforwindows.org
```

## Offline editing

Offline editing can be carried out using any simple text editor. The documents have been developed using [Atom](https://atom.io) and it is recommended to use this where possible, to avoid any conflicts and to ease support queries.

Changes can be committed to a local repository and pushed to the remote repository when ready.

This online resource explains how to use git for windows. [https://code.tutsplus.com/tutorials/git-on-windows-for-newbs--net-25847](https://code.tutsplus.com/tutorials/git-on-windows-for-newbs--net-25847)

### MkDocs guidance

The important function available in MkDocs is the use of the local server, which provides an (almost) live preview of the documentation. The server is started using the command (at the command prompt):

```
mkdocs serve
```

!!! important
    The command must be executed in the root of the project folder.

Once the server has been started the web page can be opened at [http://127.0.0.1:8000](http://127.0.0.1:8000).

### Git guidance

Detailed information on how to use Git will not be provided here. There is plenty of [information](https://www.nobledesktop.com/blog/what-is-git-and-why-should-you-use-it) on the web.

This section will be developed to include specific use of the repository, version control and the windows Git GUI.

!!! Important
    In order to use git for windows, or git to a remote reporsitory (GitLab) you will need an SSH key, to ensure that the desktop and remote server can communicate securely.

To set up an SSH key see step 2 [here](https://code.tutsplus.com/tutorials/git-on-windows-for-newbs--net-25847).

Git uses the concept of branches to manage contributions to code/data. More information can be found [here](https://www.nobledesktop.com/blog/what-is-git-and-why-should-you-use-it).

Authors of the AGS documents should create a new branch using their name as the branch name. Following a successful merge request into the master or dev branch the branch should be deleted.


### Markdown guidance

A complete, detailed guidance is not provided here. There is adequate information on the MkDocs and material web sites.

Specific use of Markdown will be included here as necessary.

### Plantuml

Plantuml is used to create UML diagrams, which are defined in a text file and rendered as SVG on the fly.

### Blockdiag

Blockdiag is used to create simple relationship diagrams. It doesn't have the flexibility of Plantuml, but the high quality of presentation is useful for inline diagrams.



To be developed......
