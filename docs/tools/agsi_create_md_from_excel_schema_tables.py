# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 21:16:33 2019

@author: neil.chadwick


"""

#TODO
#obj names italics missed in a few plages when in text
# other sheets
#where do we put extra guidance?

import openpyxl
import os
from tkinter import Tk, filedialog

# Hardcode filenames for now
#pathroot = 'C:\\Users\\Neil.Chadwick\\Documents\\DIGITAL\\Data models\\AGSi working\\documentation\\'
#inputfile = pathroot + 'AGSi_schema_draft8_agsModel.xlsx'

#Get input file
root = Tk()
inputfile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select input file',
                                    filetypes = (('Excel files','*.xls'),
                                                 ('Excel files','*.xlsx')))
#inputfilename='C:\\Users\\Neil.Chadwick\\Documents\\DIGITAL\\Data models\\AGSi working\\AGSi_schema_database_agsData.xlsx'
#Correct file name hard way until fix found
inputfile = inputfile.replace('/','\\')
inputfolder = os.path.dirname(inputfile)
# Select folder to save as (will prompt if already exists)
outputfolder=filedialog.askdirectory(initialdir = '',
                                   title = 'Folder for md files created')
root.destroy()  # Gets rid of annoying Tk window

#outputfolder = pathroot + 'testmd\\'
#outputfilenameroot = 'Model_'


#Read workbook
wb = openpyxl.load_workbook(inputfile, data_only=True)
sheetnames = wb.sheetnames
# Read each sheet
for sheetname in sheetnames:
    outputtxt = '' #Initialise text back to blank
    txt = [] # Reset temp text array
    ws = wb[sheetname]
    if sheetname.startswith('ags'):  # If starts with ags then process as attribute table
        # What Group is it?
        if sheetname.startswith('agsiModel'):
            Group = 'Model'
        elif sheetname.startswith('agsiGeometry'):
            Group = 'Geometry'
        elif sheetname.startswith('agsiData'):
            Group = 'Data'       
        outputfilenameroot = Group + '_' # used later
        # Write top title lines
        txt.append('# ' + Group + '\n')
        txt.append('## ' + 'Object model' + '\n')
        txt.append('### ' + sheetname + '\n')
        # 
        #agsobj = '*' + sheetname + '*'  # not sure we need this any more
        agsobj = sheetname
        # In the follwing, check col 1 is as expected - ignore if not
        if str(ws.cell(row=2, column=1).value).lower().startswith('description'):
            txt.append(str(ws.cell(row=2, column=2).value) + '\n')
        if str(ws.cell(row=3, column=1).value).lower().startswith('parent'):
            parent = str(ws.cell(row=3, column=2).value)
            if parent != 'None':
                txt.append('The parent object of ' + agsobj + ' is ' + parent + '\n')
        if str(ws.cell(row=4, column=1).value).lower().startswith('child'):
            child = str(ws.cell(row=4, column=2).value)
            if child != 'None':
                child = child.replace(', ',',')
                childlist = child.split(',')
                temptxt = ' contains the following embedded child objects: '
                txt.append(agsobj + temptxt + '\n\n- ' + '\n- '.join(childlist) + '\n')
        if str(ws.cell(row=5, column=1).value).lower().startswith('association'):
            assoc = str(ws.cell(row=5, column=2).value)
            if assoc != 'None':
                assoc = assoc.replace(', ',',')
                assoclist = assoc.split(',')
                temptxt = ' has associations (reference links) with the following objects: '
                txt.append(agsobj + temptxt + '\n\n- ' + '\n- '.join(assoclist) + '\n')    
        if str(ws.cell(row=6, column=1).value).lower().startswith('attribute'):
            # This is the header row for the table, triggers table loop
            # For now we assume correct order of columns!
            #Get first attribute
            txt.append(agsobj + ' has the following additional attributes:\n\n')
            r = 7
            attr = str(ws.cell(row=r, column=1).value)
            while attr != 'None':  #Do until blank attribute name reached!
                atype = str(ws.cell(row=r, column=2).value)
                addreq = str(ws.cell(row=r, column=3).value)
                isreq = str(ws.cell(row=r, column=4).value)
                desc = str(ws.cell(row=r, column=5).value)
                example = str(ws.cell(row=r, column=6).value)
                temptxt = '#### '+ attr + '\n'
                temptxt = temptxt + desc + '  \n'
                temptxt = temptxt + '*Type:* ' + atype 
                if addreq != 'None':
                    temptxt = temptxt + ', ' + addreq
                temptxt = temptxt + '  \n'
                if isreq != 'None':
                    temptxt = temptxt + '*Condition:* ' + isreq + '  \n'
                if example != 'None':
                    temptxt = temptxt + '*Example:* ``' + example + '``'   
                txt.append(temptxt +'\n')
                r = r + 1
                attr = str(ws.cell(row=r, column=1).value)
                 
    
    # Compile to md and save as text file
    txt.append('')
    outputtxt = outputtxt + '\n' + '\n'.join(txt)
    outputfilename = outputfilenameroot + sheetname + '.md'
    outputfile = os.path.join(outputfolder, outputfilename)
    f = open(outputfile, 'w')
    f.write(outputtxt)
    f.close()

